#include "Parapol.h"


void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	//draw 2 points
	SDL_RenderDrawPoint(ren, xc - x, y + yc);
	SDL_RenderDrawPoint(ren, xc + x, y + yc);
	
	
	
}
void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	//Area 1
	int x, y, p, bound = 300;
	x = 0;
	y = 0;
	p = 1 - A;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A){
		if (p <= 0){
			p += 2 * x + 3;
		}
		else{
			p += 2 * x + 3 - 2 * A;
			y++;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}

	// Area 2
	p = 2 * A - 1;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= bound){
		if (p <= 0){
			p += 4 * A;

		}
		else{
			p += 4 * A - 4 * x - 4;
			x++;
		}
		y++;
		Draw2Points(xc, yc, x, y, ren);
	}
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	//Area 1
	int x, y, p, bound = 300;
	x = 0;
	y = 0;
	p = 1 - A;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= A){
		if (p <= 0){
			p += 2 * x + 3;
		}
		else{
			p += 2 * x + 3 - 2 * A;
			y--;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}

	// Area 2
	p = 2 * A - 1;
	Draw2Points(xc, yc, x, y, ren);
	while (x <= bound){
		if (p <= 0){
			p += 4 * A;

		}
		else{
			p += 4 * A - 4 * x - 4;
			x++;
		}
		y--;
		Draw2Points(xc, yc, x, y, ren);
	}
	
}
