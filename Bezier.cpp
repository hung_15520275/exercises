#include "Bezier.h"
#include <iostream>
using namespace std;

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	double x = 0, y = 0.0, t = 0.0;
	int i = 0;
	for (t = 0.0; t <= 1.0; t += 0.0001) // increating value by 0.0001
	{
		x = pow(1 - t, 2)*p1.x + 2 * t*(1 - t)*p2.x + pow(t, 2)*p3.x;
		y = pow(1 - t, 2)*p1.y + 2 * t*(1 - t)*p2.y + pow(t, 2)*p3.y;
		SDL_RenderDrawPoint(ren, (int)x, (int)y);
	}

}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	double x = 0, y = 0.0, t = 0.0;
	int i = 0;
	for (t = 0.0; t <= 1.0; t += 0.0001) // increating value by 0.0001
	{
		x = pow(1 - t, 3)*p1.x + 3 * t*pow(1 - t, 2)*p2.x + 3 * pow(t, 2)*(1 - t)*p3.x + pow(t, 3)*p4.x;
		y = pow(1 - t, 3)*p1.y + 3 * t*pow(1 - t, 2)*p2.y + 3 * pow(t, 2)*(1 - t)*p3.y + pow(t, 3)*p4.y;
		SDL_RenderDrawPoint(ren, (int)x, (int)y);
	}


}


