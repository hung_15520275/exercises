#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);

}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	float p, a2, b2;
	int x, y;
	a2 = a*a;
	b2 = b*b;
	x = 0;
	y = b;
	p = 2 * ((float)b2 / a2) - 2 * b + 1;
	Draw4Points(xc, yc, x, y, ren);
	while ((float)b2 / a2*x <= y){
		if (p < 0){
			p += 2 * ((float)b2 / a2)*(2 * x + 3);
		}
		else{
			p += -4 * y + 2 * ((float)b2 / a2)*(2 * x + 3);
			y--;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}
	// Area 2
	y = 0;
	x = a;
	p = 2 * ((float)a2 / b2) - 2 * a + 1;
	Draw4Points(xc, yc, x, y, ren);
	while (((float)a2 / b2)*y <= x){
		if (p < 0){

			p += 2 * ((float)a2 / b2)*(2 * y + 3);
		}
		else{
			p += -4 * x + 2 * ((float)a2 / b2)*(2 * y + 3);
			x--;
		}
		y++;
		Draw4Points(xc, yc, x, y, ren);

    }
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	int x, y;
	float p;
	x = 0;
	y = b;
	Draw4Points(xc, yc, x, y, ren);
	p = a*a - 4 * a*a*b + 4 * b*b;
	while ((a*a + b*b)*x*x <= a*a*a*a){
		if (p < 0){
			p += (8 * b*b*x + 12 * b*b);
		}
		else {
			p += (8 * b*b*x - 8 * a*a*y + 8 * a*a + 12 * b*b);
			y--;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);

	}
	// Area 2
	while (y >= 0){
		if (p < 0){
			p += (8 * b*b*x - 8 * a*a*y + 12 * a*a + 8 * b*b);
			x++;
		}
		else{
			p += (-8 * a*a*y + 12 * a*a);
		}
		y--;
		Draw4Points(xc, yc, x, y, ren);
	}

	

}

