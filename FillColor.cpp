#include "FillColor.h"
#include <queue>
#include <stack>
#include <iostream>
using namespace std;

#define INT_SIZE sizeof(Uint32) * 8 /* Integer size in bits */


int findHighestBitSet(Uint32 num)
{
	int order = 0, i;

	/* Iterate over each bit of integer */
	for (i = 0; i < INT_SIZE; i++)
	{
		/* If current bit is set */
		if ((num >> i) & 1)
			order = i;
	}

	return order;
}

//Get color of a pixel
SDL_Color getPixelColor(Uint32 pixel_format, Uint32 pixel)
{
	SDL_PixelFormat* fmt = SDL_AllocFormat(pixel_format);

	Uint32 temp;
	Uint8 red, green, blue, alpha;

	//Check if pixel is a 32-bit integer
	if (findHighestBitSet(pixel) == 31)
	{
		/* Get Alpha component */
		temp = pixel & fmt->Amask;  /* Isolate alpha component */
		temp = temp >> fmt->Ashift; /* Shift it down to 8-bit */
		temp = temp << fmt->Aloss;  /* Expand to a full 8-bit number */
		alpha = (Uint8)temp;
	}
	else {
		alpha = 255;
	}

	/* Get Red component */
	temp = pixel & fmt->Rmask;  /* Isolate red component */
	temp = temp >> fmt->Rshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Rloss;  /* Expand to a full 8-bit number */
	red = (Uint8)temp;

	/* Get Green component */
	temp = pixel & fmt->Gmask;  /* Isolate green component */
	temp = temp >> fmt->Gshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Gloss;  /* Expand to a full 8-bit number */
	green = (Uint8)temp;

	/* Get Blue component */
	temp = pixel & fmt->Bmask;  /* Isolate blue component */
	temp = temp >> fmt->Bshift; /* Shift it down to 8-bit */
	temp = temp << fmt->Bloss;  /* Expand to a full 8-bit number */
	blue = (Uint8)temp;


	SDL_Color color = { red, green, blue, alpha };
	return color;

}

//Get all pixels on the window
SDL_Surface* getPixels(SDL_Window* SDLWindow, SDL_Renderer* SDLRenderer) {



	SDL_Surface* saveSurface = NULL;
	SDL_Surface* infoSurface = NULL;
	infoSurface = SDL_GetWindowSurface(SDLWindow);

	if (infoSurface == NULL) {
		std::cerr << "Failed to create info surface from window in saveScreenshotBMP(string), SDL_GetError() - " << SDL_GetError() << "\n";
		return NULL;
	}
	else {
		unsigned char * pixels = new (std::nothrow) unsigned char[infoSurface->w * infoSurface->h * infoSurface->format->BytesPerPixel];
		if (pixels == 0) {
			std::cerr << "Unable to allocate memory for screenshot pixel data buffer!\n";
			return NULL;
		}
		else {
			if (SDL_RenderReadPixels(SDLRenderer, &infoSurface->clip_rect, infoSurface->format->format, pixels, infoSurface->w * infoSurface->format->BytesPerPixel) != 0) {
				std::cerr << "Failed to read pixel data from SDL_Renderer object. SDL_GetError() - " << SDL_GetError() << "\n";
				return NULL;
			}
			else {
				saveSurface = SDL_CreateRGBSurfaceFrom(pixels, infoSurface->w, infoSurface->h, infoSurface->format->BitsPerPixel, infoSurface->w * infoSurface->format->BytesPerPixel, infoSurface->format->Rmask, infoSurface->format->Gmask, infoSurface->format->Bmask, infoSurface->format->Amask);
			}
		}
	}
	return saveSurface;
}

//Compare two colors
bool compareTwoColors(SDL_Color color1, SDL_Color color2)
{
	if (color1.r == color2.r && color1.g == color2.g && color1.b == color2.b && color1.a == color2.a)
		return true;
	return false;
}


bool canFilled(SDL_Window *win, Vector2D newPoint, Uint32 pixel_format,
	SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{


	SDL_Surface *surface = getPixels(win, ren);
	//Convert the pixels to 32 bit
	Uint32 *pixels = (Uint32 *)surface->pixels;
	int w = surface->w;

	int index = newPoint.y * w + newPoint.x;
	Uint32 pixel = pixels[index];
	SDL_Color color = getPixelColor(pixel_format, pixel);
	//cout << (int)color.r << "," << (int)color.g << "," << (int)color.b << "," << (int)color.a << endl;

	if (!compareTwoColors(color, fillColor) && !compareTwoColors(color, boundaryColor))
	{
		return true;
	}

	return false;




}

void BoundaryFill4(SDL_Window *win, Vector2D startPoint, Uint32 pixel_format,
	SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{

	queue<Vector2D> vt;
	vt.push(Vector2D(startPoint));
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	SDL_RenderDrawPoint(ren, startPoint.x, startPoint.y);
	Vector2D v;
	while (!vt.empty()){
		v.x = vt.front().x - 1;
		v.y = vt.front().y;
		if (canFilled(win, v, pixel_format, ren, fillColor, boundaryColor)){
			vt.push(Vector2D(v));
			SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
			SDL_RenderDrawPoint(ren, v.x, v.y);

		}
		v.x = vt.front().x + 1;
		v.y = vt.front().y;
		if (canFilled(win, v, pixel_format, ren, fillColor, boundaryColor))
		{

			vt.push(Vector2D(v));
			SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
			SDL_RenderDrawPoint(ren, v.x, v.y);

		}
		v.x = vt.front().x;
		v.y = vt.front().y - 1;
		if (canFilled(win, v, pixel_format, ren, fillColor, boundaryColor))
		{

			vt.push(Vector2D(v));
			SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
			SDL_RenderDrawPoint(ren, v.x, v.y);

		}
		v.x = vt.front().x;
		v.y = vt.front().y + 1;
		if (canFilled(win, v, pixel_format, ren, fillColor, boundaryColor))
		{
			vt.push(Vector2D(v));
			SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
			SDL_RenderDrawPoint(ren, v.x, v.y);

		}
		vt.pop();


	}

}

//======================================================================================================================
//=============================================FILLING TRIANGLE=========================================================

int maxIn3(int a, int b, int c)
{
	int lc = a;
	if (b > lc)
		c = b;
	if (c > lc)
		lc = c;
	return lc;
}

int minIn3(int a, int b, int c)
{
	int lc = a;
	if (b < lc)
		c = b;
	if (c < lc)
		lc = c;
	return lc;
}

void swap(Vector2D &a, Vector2D &b)
{
	Vector2D temp(a);
	a.set(b);
	b.set(temp);
}

void ascendingSort(Vector2D &v1, Vector2D &v2, Vector2D &v3)
{
	if (v1.y > v2.y) {
		swap(v1, v2);
	}

	if (v1.y > v3.y)
	{
		swap(v1, v3);
	}

	if (v2.y > v3.y)
	{
		swap(v2, v3);
	}
}

void TriangleFill1(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	int xmin = minIn3(v1.x, v2.x, v3.x);
	int xmax = maxIn3(v1.x, v2.x, v3.x);
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	DDA_Line(xmin, v1.y, xmax, v1.y, ren);

}

void TriangleFill2(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float c1 = (float)(v3.x - v1.x) / (v3.y - v1.y);
	float c2 = (float)(v3.x - v2.x) / (v3.y - v2.y);
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	float xLeft = v1.x;
	float xRight = v2.x;
	for (int y = v1.y; y <= v3.y; y++)
	{
		xLeft += c1;
		xRight += c2;
		DDA_Line(int(xLeft + 0.5), y, int(xRight + 0.5), y, ren);
	}

}

void TriangleFill3(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float c1 = (float)(v2.x - v1.x) / (v2.y - v1.y);
	float c2 = (float)(v3.x - v1.x) / (v3.y - v1.y);
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	float xLeft = v1.x;
	float xRight = v1.x;
	for (int y = v1.y; y <= v3.y; y++)
	{
		xLeft += c1;
		xRight += c2;
		DDA_Line(int(xLeft + 0.5), y, int(xRight + 0.5), y, ren);
	}

}

void TriangleFill4(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	float c1 = (float)(v2.x - v1.x) / (v2.y - v1.y);
	float c2 = (float)(v3.x - v1.x) / (v3.y - v1.y);
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	float xLeft = v1.x;
	float xRight = v1.x;
	for (int y = v1.y; y <= v2.y; y++)
	{
		xLeft += c1;
		xRight += c2;
		DDA_Line(int(xLeft + 0.5), y, int(xRight + 0.5), y, ren);
	}
	c1 = (float)(v3.x - v2.x) / (v3.y - v2.y);
	for (int y = v2.y; y <= v3.y; y++)
	{
		xLeft += c1;
		xRight += c2;
		DDA_Line(int(xLeft + 0.5), y, int(xRight + 0.5), y, ren);
	}

}

void TriangleFill(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	ascendingSort(v1, v2, v3);
	if (v1.y == v2.y && v2.y == v3.y)
	{
		TriangleFill1(v1, v2, v3, ren, fillColor);
		return;
	}
	if (v1.y == v2.y && v2.y < v3.y)
	{
		TriangleFill2(v1, v2, v3, ren, fillColor);
		return;
	}
	if (v1.y < v2.y && v2.y == v3.y)
	{
		TriangleFill3(v1, v2, v3, ren, fillColor);
		return;
	}
	TriangleFill4(v1, v2, v3, ren, fillColor);
}
//======================================================================================================================
//===================================CIRCLE - RECTANGLE - ELLIPSE=======================================================
bool isInsideCircle(int xc, int yc, int R, int x, int y)
{
	if ((x - xc)*(x - xc) + (y - yc)*(y - yc) <= R*R)
		return 1;
	return 0;
}

int Max(int a, int b)
{
	if (a > b)
		return a;
	return b;
}
int Min(int a, int b)
{
	if (a < b)
		return a;
	return b;
}

void FillIntersection(int x1, int y1, int x2, int y2, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int xmin = Min(x1, x2);
	int xmax = Max(x1, x2);
	for (int x = xmin; x <= xmax; x++)
		if (isInsideCircle(xc, yc, R, x, y1) == 1)
		{
			SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
			SDL_RenderDrawPoint(ren, x, y1);
		}
}

void FillIntersectionRectangleCircle(Vector2D vTopLeft, Vector2D vBottomRight, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	for (int y = vTopLeft.y; y <= vBottomRight.y; y++){
		FillIntersection(vTopLeft.x, y, vBottomRight.x, y, xc, yc, R, ren, fillColor);

	}
}

void RectangleFill(Vector2D vTopLeft, Vector2D vBottomRight, SDL_Renderer *ren, SDL_Color fillColor)
{

	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	for (int y = vTopLeft.y; y <= vBottomRight.y; y++)
		DDA_Line(vTopLeft.x, y, vBottomRight.x, y, ren);
}

void put4line(int xc, int yc, int x, int y, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	DDA_Line(xc + x, yc + y, xc - x, yc + y, ren);
	DDA_Line(xc + y, yc + x, xc - y, yc + x, ren);
	DDA_Line(xc + y, yc - x, xc - y, yc - x, ren);
	DDA_Line(xc + x, yc - y, xc - x, yc - y, ren);
}

void CircleFill(int xc, int yc, int R, SDL_Renderer *ren, SDL_Color fillColor)
{
	int x = 0;
	int y = R;
	int p = 1 - R;
	put4line(xc, yc, x, y, ren, fillColor);
	while (x < y){
		if (p < 0)
		{
			p += 2 * x + 3;
		}
		else {
			p += 2 * (x - y) + 5;
			y = y - 1;
		}
		x++;
		put4line(xc, yc, x, y, ren, fillColor);
	}
}
void Put_4line(int xc1, int yc1, int xc2, int yc2, int R2, int x, int y, SDL_Renderer *ren, SDL_Color fillColor)
{
	FillIntersection(xc1 + x, yc1 + y, xc1 - x, yc1 + y, xc2, yc2, R2,
		ren, fillColor);
	FillIntersection(xc1 + y, yc1 + x, xc1 - y, yc1 + x, xc2, yc2, R2,
		ren, fillColor);
	FillIntersection(xc1 + y, yc1 - x, xc1 - y, yc1 + x, xc2, yc2, R2,
		ren, fillColor);
	FillIntersection(xc1 + x, yc1 - y, xc1 - x, yc1 - y, xc2, yc2, R2,
		ren, fillColor);
}

int isCheck(int xc, int yc, int a, int b, int x, int y)
{
	float s1 = (float)(x - xc) * (x - xc) / (a*a);
	float s2 = (float)(y - yc)*(y - yc) / (b*b);
	float kq = s1 + s2;
	if (kq <= 1)
		return 1;
	return 0;
}
void fillIntersection(int x1, int y1, int x2, int y2,
	SDL_Renderer *ren, SDL_Color fillColor,
	int xe, int ye, int a, int b)
{
	int xmin = Min(x1, x2);
	int xmax = max(x1, x2);
	for (int x = xmin; x <= xmax; x++)
		if (isCheck(xe, ye, a, b, x, y1) == 1)
		{
			SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
			SDL_RenderDrawPoint(ren, x, y1);
		}
}

void Put4_Line(int xc, int yc, int x, int y,
	SDL_Renderer *ren, SDL_Color fillColor,
	int xe, int ye, int a, int b)
{
	fillIntersection(xc + x, yc + y, xc - x, yc + y,
		ren, fillColor, xe, ye, a, b);
	fillIntersection(xc + y, yc + x, xc - y, yc + x,
		ren, fillColor, xe, ye, a, b);
	fillIntersection(xc + y, yc - x, xc - y, yc - x,
		ren, fillColor, xe, ye, a, b);
	fillIntersection(xc + x, yc - y, xc - x, yc - y,
		ren, fillColor, xe, ye, a, b);
}

void FillIntersectionEllipseCircle(int xcE, int ycE, int RE, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int a = 50;
	int b = 50;
	int x = 0;
	int y = R;
	int p = 1 - R;
	Put4_Line(xc, yc, x, y, ren, fillColor, xcE, ycE, a, b);
	while (x < y)
	{
		if (p < 0)
			p += 2 * x + 3;
		else
		{
			p += 2 * (x - y) + 5;
			y--;
		}
		x++;
		Put4_Line(xc, yc, x, y, ren, fillColor, xcE, ycE, a, b);
	}

}


void FillIntersectionTwoCircles(int xc1, int yc1, int R1, int xc2, int yc2, int R2,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int x = 0;
	int y = R1;
	int p = 1 - R1;
	Put_4line(xc1, yc1, xc2, yc2, R2, x, y, ren, fillColor);
	while (x < y)
	{
		if (p < 0)
			p += 2 * x + 3;
		else
		{
			p += 2 * (x - y) + 5;
			y--;
		}
		x++;
		Put_4line(xc1, yc1, xc2, yc2, R2, x, y, ren, fillColor);
	}
}



