#include <iostream>
#include <SDL.h>
#include<vector>
#include "Bezier.h"

using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;

SDL_Event event;

void drawRect(SDL_Renderer *ren, SDL_Color colorRect, SDL_Rect rect[4], Vector2D points[4],int pointsControl)
{
	for (int j = 0; j < pointsControl -1; j++)
	{
		/* drawing rect1  control Point P1*/
		SDL_SetRenderDrawColor(ren, colorRect.r, colorRect.g, colorRect.b, colorRect.a);
		SDL_RenderDrawRect(ren, &rect[j]);
		
		/*Red Line between control Point P1 & P2*/
		SDL_SetRenderDrawColor(ren, 255, 0, 0, 255);
		SDL_RenderDrawLine(ren, points[j].x, points[j].y, points[j + 1].x, points[j + 1].y);
	}
	SDL_SetRenderDrawColor(ren, colorRect.r, colorRect.g, colorRect.b, colorRect.a);
	SDL_RenderDrawRect(ren, &rect[pointsControl - 1]);
}

int main(int, char**){
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL){
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL){
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE

	Vector2D p1(22, 215), p2(94, 43), p3(140, 258), p4(213, 150);


	SDL_Rect *rect1 = new SDL_Rect();
	rect1->x = p1.x - 3;
	rect1->y = p1.y - 3;
	rect1->w = 6;
	rect1->h = 6;

	SDL_Rect *rect2 = new SDL_Rect();
	rect2->x = p2.x - 3;
	rect2->y = p2.y - 3;
	rect2->w = 6;
	rect2->h = 6;

	SDL_Rect *rect3 = new SDL_Rect();
	rect3->x = p3.x - 3;
	rect3->y = p3.y - 3;
	rect3->w = 6;
	rect3->h = 6;

	SDL_Rect *rect4 = new SDL_Rect();
	rect4->x = p4.x - 3;
	rect4->y = p4.y - 3;
	rect4->w = 6;
	rect4->h = 6;

	SDL_Color colorCurve = { 100, 20, 40, 255 }, colorRect = { 0, 255, 40, 255 };


	//Take a quick break after all that hard work
	//Quit if happen QUIT event
	bool running = true;
	int i = 0;
	int mousePoseX, mousePoseY;
	Vector2D points[4];
	Vector2D po[4];
	SDL_Rect rect[4];
	SDL_Rect r[4];
	int flag = 0;
	int pointsControl;
	int xnew, ynew;
	int xnew1, ynew1, mousePoseX1, mousePoseY1;
	points[0] = p1;
	points[1] = p2;
	points[2] = p3;
	points[3] = p4;
	rect[0] = *rect1;
	rect[1] = *rect2;
	rect[2] = *rect3;
	rect[3] = *rect4;

	po[0].x = p1.x + 200; po[0].y = p1.y + 200;
	po[1].x = p2.x + 200; po[1].y = p2.y + 200;
	po[2].x = p3.x + 200; po[2].y = p3.y + 200;
	r[0].x = rect1->x +200; r[0].y = rect1->y+200; r[0].w = 6; r[0].h = 6;
	r[1].x = rect2->x + 200; r[1].y = rect2->y +200; r[1].w = 6; r[1].h = 6;
	r[2].x = rect3->x + 200; r[2].y = rect3->y + 200; r[2].w = 6; r[2].h = 6;

	
	while (running)
	{
		//YOU CAN INSERT CODE FOR TESTING HERE

		SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
		SDL_RenderClear(ren);
		SDL_SetRenderDrawColor(ren, 123, 200, 125, 255);

		pointsControl = 4;
		DrawCurve3(ren, points[0], points[1], points[2], points[3]);
		drawRect(ren, colorRect, rect, points, pointsControl);
		
		pointsControl = 3;
		DrawCurve2(ren, po[0], po[1], po[2]);
		drawRect(ren, colorRect, r, po, pointsControl);
		

		//If there's events to handle
		if (SDL_PollEvent(&event))
		{
			//HANDLE MOUSE EVENTS!!!

			/*Mouse is in motion */
			if (event.type == SDL_MOUSEMOTION) 
			{
				//DrawCurve3
				/*get x and y postions from motion of mouse*/
				xnew = event.motion.x;
				ynew = event.motion.y;
				/* change coordinates of control point
				after bezier curve has been drawn */
				for (int j = 0; j < 4; j++)
				{
					if ((float)sqrt(abs(xnew - points[j].x)*abs(xnew - points[j].x) +
						abs(ynew - points[j].y)*abs(ynew - points[j].y)) < 8.0)
					{
						/*change coordinate of jth control point*/
						points[j].x = xnew;
						points[j].y = ynew;
						rect[j].x = xnew - 3;
						rect[j].y = ynew - 3;
					}
				}
				/*updating mouse positions to positions coming from motion*/
				mousePoseX = xnew;
				mousePoseY = ynew;


				//DrawCurve2
				xnew1 = event.motion.x;
				ynew1 = event.motion.y;
				/* change coordinates of control point
				after bezier curve has been drawn */
				for (int j = 0; j < 3; j++)
				{
					if ((float)sqrt(abs(xnew1 - po[j].x)*abs(xnew1 - po[j].x) +
						abs(ynew1 - po[j].y)*abs(ynew1 - po[j].y)) < 8.0)
					{
						/*change coordinate of jth control point*/
						po[j].x = xnew1;
						po[j].y = ynew1;
						r[j].x = xnew1 - 3;
						r[j].y = ynew1 - 3;
					}
				}
				/*updating mouse positions to positions coming from motion*/
				mousePoseX1 = xnew1;
				mousePoseY1 = ynew1;
			}
			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;

			}
		}
		SDL_RenderPresent(ren);

	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();



	return 0;
}
