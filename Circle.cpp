#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	int new_x;
	int new_y;

	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	//7 points

	SDL_RenderDrawPoint(ren, -x + xc, y + yc);
	SDL_RenderDrawPoint(ren, x + xc, -y + yc);
	SDL_RenderDrawPoint(ren, -x + xc, -y + yc);
	SDL_RenderDrawPoint(ren, y + xc, x + yc);
	SDL_RenderDrawPoint(ren, -y + xc, x + yc);
	SDL_RenderDrawPoint(ren, y + xc, -x + yc);
	SDL_RenderDrawPoint(ren, -y + xc, -x + yc);

}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0;
	int y = R;
	int p = 3 - 2 * R;
	Draw8Points(xc, yc, x, y, ren);
	while (x <= y){

		if (p < 0)
			p = p + 4 * x + 6;
		else{
			p = p + 4 * (x - y) + 10;
			y--;
		}
		x++;
		Draw8Points(xc, yc, x, y, ren);
	}
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	float p = 5 / 4 - R;
	int x = 0;
	int y = R;
	Draw8Points(xc, yc, x, y, ren);
	while (x < y){
		if (p < 0){
			p = p + 2 * x + 3;

		}
		else{
			p = p + 2 * (x - y) + 5;
			y--;
		}
		x++;
		Draw8Points(xc, yc, x, y, ren);
	}
}
